# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-07 00:45+0000\n"
"PO-Revision-Date: 2022-10-27 19:21+0700\n"
"Last-Translator: OIS <mistresssilvara@hotmail.com>\n"
"Language-Team: Interlingue <kde-i18n-doc@kde.org>\n"
"Language: ie\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.12\n"

#: kcm/package/contents/ui/main.qml:32
#, kde-format
msgid "Update software:"
msgstr "Actualisar programmas:"

#: kcm/package/contents/ui/main.qml:33
#, kde-format
msgid "Manually"
msgstr "Manualmen"

#: kcm/package/contents/ui/main.qml:43
#, kde-format
msgid "Automatically"
msgstr "Automaticmen"

#: kcm/package/contents/ui/main.qml:50
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Software updates will be downloaded automatically when they become "
"available. Updates for applications will be installed immediately, while "
"system updates will be installed the next time the computer is restarted."
msgstr ""
"Actualisamentes va esser descargat automaticmen tam bentost quam ili es "
"disponibil. Actualisamentes por programmas va esser installat automaticmen, "
"e li actualisamentes del sistema va esser installat quande li computator es "
"reinicialisat."

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Update frequency:"
msgstr "Frequentie de actualisationes:"

#: kcm/package/contents/ui/main.qml:61
#, kde-format
msgctxt "@title:group"
msgid "Notification frequency:"
msgstr "Frequentie de notificationes:"

#: kcm/package/contents/ui/main.qml:64
#, kde-format
msgctxt "@item:inlistbox"
msgid "Daily"
msgstr "Diari"

#: kcm/package/contents/ui/main.qml:65
#, kde-format
msgctxt "@item:inlistbox"
msgid "Weekly"
msgstr "Semanal"

#: kcm/package/contents/ui/main.qml:66
#, kde-format
msgctxt "@item:inlistbox"
msgid "Monthly"
msgstr "Mensual"

#: kcm/package/contents/ui/main.qml:67
#, kde-format
msgctxt "@item:inlistbox"
msgid "Never"
msgstr "Nequande"

#: kcm/package/contents/ui/main.qml:111
#, kde-format
msgid "Use offline updates:"
msgstr "Usar ajornat actualisation:"

#: kcm/package/contents/ui/main.qml:124
#, kde-format
msgid ""
"Offline updates maximize system stability by applying changes while "
"restarting the system. Using this update mode is strongly recommended."
msgstr ""
"Ajornat actualisation maximisa li stabilitá del sistema per application de "
"modificationes quande on reincialisa li sistema. Li usa de ti mode de "
"actualisation es fortmen recomandat."

#: kcm/updates.cpp:31
#, kde-format
msgid "Software Update"
msgstr "Actualisation de programmas"

#: kcm/updates.cpp:33
#, kde-format
msgid "Configure software update settings"
msgstr "Configurar parametres del actualisation de programmas"
